package com.aula3calc.calc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CalcController {
	
    @GetMapping("/")
    public ModelAndView calculator (){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("calculator");
        return modelAndView;
    }       
        
    @PostMapping("/calculate")
    public String calculate(@RequestParam("num1") double num1,
                            @RequestParam("operator") String operator,
                            Model model) {
        double result=0;
        String unit = "";
        
        switch (operator) {
        case "cf":
            result = (num1 * 9/5) + 32;
            unit = "F°";
            break;
        case "fc":
            result = (num1 - 32) * 5/9;
            unit = "C°";
            break;
        case "ck":
            result = num1 + 273.15;
            unit = "K";
            break;
        case "kc":
            result = num1 - 273.15;
            unit = "C°";
            break;
        case "kf":
            result = (num1 - 273.15) * 9/5 + 32;
            unit = "K";
            break;
        case "fk":
            result = (num1 - 32) * 5/9 + 273.15;
            unit = "F°";
            break;
        default:
            break;
    
        }
        model.addAttribute("result", String.format("%.2f %s", result, unit));
        return "calculator";
    }

	private int num1(int i) {
		// TODO Auto-generated method stub
		return 0;
	}
}